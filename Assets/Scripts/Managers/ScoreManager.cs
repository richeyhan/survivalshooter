﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour
{
    public static int score;                    //인스턴스가 아닌 클래스 자체에 속한다. 여러 인스턴스를 참조할 필요가 없음


    Text text;


    void Awake ()
    {
        text = GetComponent <Text> ();
        score = 0;                              //죽은 후 다시 시작할 때의 리셋을 목적으로 함
    }


    void Update ()
    {
        text.text = "Score: " + score;
    }
}
