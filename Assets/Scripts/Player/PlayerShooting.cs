﻿using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    public int damagePerShot = 20;
    public float timeBetweenBullets = 0.15f;                            //발사 속도를 빠르게 하려면 이 수치를 줄일 것.
    public float range = 100f;                                          //총알의 이동 거리


    float timer;
    Ray shootRay = new Ray();                                           //레이캐스트를 통해 총알이 무엇을 맞혔는지를 확인.
    RaycastHit shootHit;                                                //총알이 맞힌 것들을 반환.
    int shootableMask;                                                  //카메라의 floorMask처럼, 맞힐 수 있는 것들만 설정함.
    ParticleSystem gunParticles;
    LineRenderer gunLine;
    AudioSource gunAudio;
    Light gunLight;
    float effectsDisplayTime = 0.2f;                                    //총알 이펙트의 유지 시간


    void Awake ()
    {
        shootableMask = LayerMask.GetMask ("Shootable");
        gunParticles = GetComponent<ParticleSystem> ();
        gunLine = GetComponent <LineRenderer> ();
        gunAudio = GetComponent<AudioSource> ();
        gunLight = GetComponent<Light> ();
    }


    void Update ()
    {
        timer += Time.deltaTime;                            //쏠 수 있는 시간인지를 체크하기 위함

		if(Input.GetButton ("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0)
        {
            Shoot ();
        }

        if(timer >= timeBetweenBullets * effectsDisplayTime)
        {
            DisableEffects ();
        }
    }


    public void DisableEffects ()
    {
        gunLine.enabled = false;                              //오브젝트가 아닌 컴포넌트이므로 setactive가 아니다!
        gunLight.enabled = false;
    }


    void Shoot ()
    {
        timer = 0f;

        gunAudio.Play ();

        gunLight.enabled = true;

        gunParticles.Stop ();                                   //이미 파티클이 재생중이라면 멈추고
        gunParticles.Play ();                                   //새로운 파티클을 재생하라고 지시한다!

        gunLine.enabled = true;
        gunLine.SetPosition (0, transform.position);            //라인의 시작 포인트. transform.position은 총신을 의미한다.

        shootRay.origin = transform.position;                   //레이캐스트의 시작점을 총신으로 설정
        shootRay.direction = transform.forward;                 //레이캐스트의 발현 방향은 Z축 양수(forward)로 설정한다.

        if(Physics.Raycast (shootRay, out shootHit, range, shootableMask))              //out shoothit를 통해 맞힌 것을 반환하도록 설정 , range로 거리(100f)설정, 레이어 지정
        {
            EnemyHealth enemyHealth = shootHit.collider.GetComponent <EnemyHealth> ();
            if(enemyHealth != null)
            {
                enemyHealth.TakeDamage (damagePerShot, shootHit.point);
            }
            gunLine.SetPosition (1, shootHit.point);
        }
        else
        {
            gunLine.SetPosition (1, shootRay.origin + shootRay.direction * range);
        }
    }
}
