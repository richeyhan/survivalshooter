﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 6f;

    Vector3 movement;
    Animator anim;
    Rigidbody playerRigidbody;
    int floorMask;                                  //레이캐스트 설정을 위한 변수
    float camRayLength = 100f;

    private void Awake()
    {
        floorMask = LayerMask.GetMask("Floor");             //바닥에 생성하고 메쉬 렌더러 비활성화 한 obj에게 레이어 "Floor"를 설정한 후 불러옴
        anim = GetComponent<Animator>();
        playerRigidbody = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()                              //물리효과
    {
        float h = Input.GetAxisRaw("Horizontal");           //GetAxisRaw는 오직 -1, 0, 1의 값만을 가짐. 곧바로 최고 속도로 반응한다. 
        float v = Input.GetAxisRaw("Vertical");

        Move(h, v);
        Turning();
        Animating(h, v);
    }

    void Move (float h, float v)                            //FixedUpdate에서 호출해서 움직임을 콘트롤한다.
    {
        movement.Set(h, 0f, v);                             //Input값을 받아 Vector3 좌표값을 설정해준다.
        movement = movement.normalized * speed * Time.deltaTime;        //X,Z좌표 값으로 인해 대각선의 벡터값이 1.4가 되므로 1로 정규화 시킨다.
        playerRigidbody.MovePosition(transform.position + movement);    //현재 좌표에 movement의 좌표값을 더해준다.
    }

    void Turning()                                         //터닝은 키보드 입력값이 아닌 마우스 움직임이므로 파라미터가 없다.
    {
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit floorHit;

        if(Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
        {
            Vector3 playerToMouse = floorHit.point - transform.position;
            playerToMouse.y = 0f;

            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);
            playerRigidbody.MoveRotation(newRotation);
          
        }   
    }

    void Animating(float h, float v)
    {
        bool walking = h != 0f || v != 0f;
        anim.SetBool("IsWalking", walking);
    }
}
